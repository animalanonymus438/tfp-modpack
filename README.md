## Introduction
This repository is a simple tracker for the [Arma 3](https://arma3.com/) mods used by TFP .

## Current Modpack
Following is a list of all mods currently in the modpack, organised by the folder that contains them:

- @ ace_rhs_compat
  <br/>Version 1.10
- @ ace_TFP
  <br/>Version 3.13.5
- @ ACEX_TFP
  <br/>Version 3.5.4
- @ Achilles
  <br/>Version 1.3.1
- @ Advanced_Urban_Rappelling
  <br/>Version 1.0
- @ Atlas_LHD_Plus_Remastered
  <br/>Version  1.12
- @ BackpackOnChest
  <br/>Version 1.2
- @ BreachableShips
  <br/>Version: 0.4
- @ Bridge_PUNCH
  <br/>Version: 
- @ BunkerMod
  <br/>Version: 1.03
- @ CANS
  <br/>Version 0.5
- @ CBA_A3_TFP
  <br/>Version 3.15.2.201119
- @ CHVD
  <br/>Version 1.13
- @ CUP_Terrains_Core
  <br/>Version 1.16.1
- @ CUP_Terrains_CWA
  <br/>Version 1.12.0
- @ CUP_Terrains_Maps
  <br/>Version 1.16.0
- @ DUI_Squad_Radar
  <br/>Version 1.8.0
- @ EMRework
  <br/>Version 0.1.15
- @ Enhanced_Movement
  <br/>Version 0.8.4.15
- @ Extended_Fortifications_Mod
  <br/>Version 0.7.02
- @ FapovoIsland
  <br/>Version 1.6
- @ FSG
  <br/>Version: 
- @ FSOF
  <br/>Version 3.02
- @ GGE_Captive
  <br/>Version 1.03
- @ GGE_Core
  <br/>Version 1.05
- @ GGE_NVG
  <br/>Version 1.04
- @ GPNVG
  <br/>Version 1.00
- @ GRAD_Trenches
  <br/>Version 1.6.3.0
- @ ILBE
  <br/>Version: 1.07
- @ InteriorsForCUP
  <br/>Version 1.04
- @ JNSWarObj
  <br/>Version: 4.0
- @ LAMBS_Danger
  <br/>Version 2.4.4
- @ LAMBS_Suppression
  <br/>Version 1.3
- @ Lesh_Tow
  <br/>Version 1.03
- @ MBG_Killhouses
  <br/>Version: 1.0
- @ MissionItems
  <br/>Version: 1.6
- @ Mk5_LCVP
  <br/>Version: 3.002
- @ MLO
  <br/>Version: 1.19
- @ PackPress
  <br/>Version: 1.3
- @ PaddleMod
  <br/>Version: 3.7
- @ PFVL
  <br/>Version: 1.07
- @ PFVL_ACE3
  <br/>Version: 
- @ PLP_Markers
  <br/>Version: 2
- @ ProjectOPFOR
  <br/>Version 0.3.0
- @ RHSAFRF
  <br/>Version: 0.5.4
- @ RHSGREF
  <br/>Version: 0.5.4
- @ RHSSAF
  <br/>Version: 0.5.4
- @ RHSUSAF
  <br/>Version: 0.5.4
- @ RKSL_Tug
  <br/>Version: 3.000
- @ RoyalNavyUniforms
  <br/>Version: 0.3
- @ RussianWinterRetextures
  <br/>Version: 1.02
- @ SimplexSupport
  <br/>Version: 0.3.2.2
- @ Splendid_Smoke
  <br/>Version: 1.1
- @ StreetSigns
  <br/>Version: 5.0
- @ Tactical_Beard
  <br/>Version: 3.0
- @ TFAR
  <br/>version: 1.-1.0.327
- @ TFP
  <br/>Version: 
- @ TripFlare
  <br/>Version: 1.3
- @WhitePhosphorNVG
  <br/>Version: 1.03


## Mod Suggestions
New mod suggestions are to be created through our forum using the built in tools there. 

## Downloading
The modpack is available only to members of Task Force Poseidon, via our Arma3Sync repository. Details on downloading the modpack can be found on our [website](http://tfposeidon.com/forums/showthread.php?tid=35).

## Reporting Issues
Issues with the modpack may be reported to anyone in S4.

## Licensing
All mods are the property of their respective owners. Please refer to any documentation included with each mod for license details.
